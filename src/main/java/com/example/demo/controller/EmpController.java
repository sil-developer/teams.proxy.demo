package com.example.demo.controller;

import com.example.demo.service.IEmpService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class EmpController {
    private @Autowired
    IEmpService empService;

    @ResponseBody
    @GetMapping("/all")
    public String getEmpList() {
        Gson gson = new Gson();
        return gson.toJson(empService.findAllEmps());
    }
}
