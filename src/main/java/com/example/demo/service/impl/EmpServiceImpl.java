package com.example.demo.service.impl;

import com.example.demo.model.Emp;
import com.example.demo.repository.EmpRepository;
import com.example.demo.service.IEmpService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmpServiceImpl implements IEmpService {
    private EmpRepository empRepository;

    public EmpServiceImpl(EmpRepository empRepository) {
        this.empRepository = empRepository;
    }

    @Override
    public Optional<Emp> findByEmpNo(Integer empNo) {

        Optional<Emp> optionalEmpDTO =  empRepository.findById(empNo);
        return optionalEmpDTO;
    }

    @Override
    public List<Emp> findAllEmps() {
        List<Emp> empList = empRepository.findAll();
        for(Emp e : empList) {
            System.out.println(e.getDeptNo());
            System.out.println(e.getComm());
        }
        return empList;
    }
}
