package com.example.demo.service;

import com.example.demo.model.Emp;

import java.util.List;
import java.util.Optional;

public interface IEmpService {
    Optional<Emp> findByEmpNo(Integer empNo);
    List<Emp> findAllEmps();
}
