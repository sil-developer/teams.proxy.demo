package com.example.demo.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "EMP")
public class Emp {
    @Id
    @GeneratedValue
    @Column(name = "EMPNO", length = 4)
    private Integer empNo;

    @Column(name = "ENAME", length = 100)
    private String ename;

    @Column(name = "JOB", length = 50)
    private String job;

    @Column(name = "MGR", length = 4)
    private Integer mgr;

    @Column(name = "HIREDATE")
    private LocalDateTime regDate;

    @Column(name = "SAL", length=2)
    private Integer sal;

    @Column(name = "COMM", length=2)
    private Integer comm;

    @Column(name = "DEPTNO", length=2)
    private Integer deptNo;

}
