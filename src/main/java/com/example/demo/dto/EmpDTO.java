package com.example.demo.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class EmpDTO {
    private Integer empNo;
    private String ename;
    private String job;
    private Integer mgr;
    private LocalDateTime regDate;
    private Integer deptNo;
}
